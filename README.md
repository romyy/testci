## Requirements 
Tout ce qui est necessaire pour le fonctionnement de l'application

```Python
    pip install -r requirements.txt 
```

le fichier requirements.txt contient tout ce qui est nécessaire pour le fonction de l'appli, pas besoin d'installer chaque framework ou outil a part. Par exemple Flask existe déja dans requirement.txt en lancant la commande ci-dessus tout sera installé d'un coup. 
## databases
 Dans le dossier database il y a un dump sql et deux images qui expliquent les deux étapes pour restorer la database.


 ## configuration dans .env  
 Dans .env mettez vos informations de connexion à la BD. Exemple : USER=youruser -> remplacer youruser par votre nom utilisateur.


 ## Rmarques :
 Les templates (html, css ...) que j'utilise c'est juste pour tester mon backend, à terme ils seront remplacées par les templates de Romy et Randy 